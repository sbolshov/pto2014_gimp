#include "morphological_operator.h"

MorphologicalOperator::MorphologicalOperator(PNM* img) :
    Transformation(img)
{
}

MorphologicalOperator::MorphologicalOperator(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

// abstract
const int MorphologicalOperator::morph(math::matrix<float>, math::matrix<bool>)
{
    return 0;
}

math::matrix<bool> MorphologicalOperator::getSE(int size, SE shape)
{
    switch (shape)
    {
    case Square:    return seSquare(size);
    case Cross:     return seCross(size);
    case XCross:    return seXCross(size);
    case VLine:     return seVLine(size);
    case HLine:     return seHLine(size);
    default:        return seSquare(size);
    }
}


math::matrix<bool> MorphologicalOperator::seSquare(int size)
{
    math::matrix<bool> ret(size, size);

	for (int x = 0; x < size; x++)
		for (int y = 0; y < size; y++)
			ret(x, y) = 1;
    return ret;
}

math::matrix<bool> MorphologicalOperator::seCross(int size)
{
    math::matrix<bool> ret(size, size);
	int axis = (int)size / 2 + 1;
	for (int x = 0; x < size; x++)
		for (int y = 0; y < size; y++)
			if ((x == axis) || (y == axis)) ret(x, y) = 1;
			else ret(x, y) = 0;
    return ret;
}

math::matrix<bool> MorphologicalOperator::seXCross(int size)
{
    math::matrix<bool> ret(size, size);
	for (int x = 0; x < size; x++)
		for (int y = 0; y < size; y++)
			if (x == y)
			{
				ret(x, y) = 1;
				ret(size - x, size - y) = 1;
			}
			else ret(x, y) = 0;
    return ret;
}

math::matrix<bool> MorphologicalOperator::seVLine(int size)
{
    math::matrix<bool> ret(size, size);
	int axis = (int)size / 2 + 1;
	for (int x = 0; x < size; x++)
		for (int y = 0; y < size; y++)
			if (y == axis) ret(x, y) = 1;
			else ret(x, y) = 0;
    return ret;
}

math::matrix<bool> MorphologicalOperator::seHLine(int size)
{
    math::matrix<bool> ret(size, size);
	int axis = (int)size / 2 + 1;
	for (int x = 0; x < size; x++)
		for (int y = 0; y < size; y++)
			if (x == axis) ret(x, y) = 1;
			else ret(x, y) = 0;
    return ret;
}

PNM* MorphologicalOperator::transform()
{  
    int size  = getParameter("size").toInt();
    SE  shape = (MorphologicalOperator::SE) getParameter("shape").toInt();

    PNM* newImage = new PNM(image->width(), image->height(), QImage::Format_RGB32);
	int width = image->width();
	int height = image->height();
	math::matrix<bool> matriks = MorphologicalOperator::getSE(size, shape);

	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++){
		
		QRgb pixel = image->pixel(x, y);
		int r = qRed(pixel);
		int g = qGreen(pixel);
		int b = qBlue(pixel);
		
		math::matrix<float> windR = getWindow(x, y, size, RChannel, NullEdge);
		math::matrix<float> windG = getWindow(x, y, size, GChannel, NullEdge);
		math::matrix<float> windB = getWindow(x, y, size, BChannel, NullEdge);

		r = morph(windR, matriks);
		g = morph(windG, matriks);
		b = morph(windB, matriks);

		newImage->setPixel(x, y, QColor(r, g, b).rgb());
		}
    

    return newImage;
}
