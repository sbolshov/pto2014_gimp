#include "bin_iterbimodal.h"
#include "histogram_equalization.h"
#include "../histogram.h"

BinarizationIterBimodal::BinarizationIterBimodal(PNM* img) :
    Transformation(img)
{
}

BinarizationIterBimodal::BinarizationIterBimodal(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationIterBimodal::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

	HistogramEqualization* eq = new HistogramEqualization(image);
	image = eq->getImage();

	int t = 128, tnew = 0, mi1, mi2, suma1, suma2;

	while (t != tnew)
	{
		mi1 = 0;
		mi2 = 0;
		suma1 = 0;
		suma2 = 0;

		t = tnew;
		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				QRgb pixel = image->pixel(x, y);
				int v = qGray(pixel);
				
				if (v > t)
				{
					mi1 += v;
					suma1++;
				}
				else
				{
					mi2 += v;
					suma2++;
				}
			}
		}

		if (suma1 != 0) mi1 = mi1 / suma1;
		if (suma2 != 0) mi2 = mi2 / suma2;
		t = tnew;
		tnew = (int)(mi1 + mi2) / 2;
		
	}

	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			QRgb pixel = image->pixel(x, y);
			int v = qGray(pixel);
			if (v>t) newImage->setPixel(x, y, 1);
			else newImage->setPixel(x, y, 0);
		}
	}
	

    return newImage;
}



