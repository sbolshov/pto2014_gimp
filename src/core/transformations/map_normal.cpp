#include "map_normal.h"

#include "edge_sobel.h"
#include "map_height.h"

MapNormal::MapNormal(PNM* img) :
    Convolution(img)
{
}

MapNormal::MapNormal(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* MapNormal::transform()
{
    int width  = image->width(),
        height = image->height();

    double strength = getParameter("strength").toDouble();

    PNM* newImage = new PNM(width, height, image->format());
	PNM* mapHeightImage = MapHeight(image).transform();
	math::matrix<float>* gx = EdgeSobel(mapHeightImage).rawHorizontalDetection();
	math::matrix<float>* gy = EdgeSobel(mapHeightImage).rawVerticalDetection();

	for (int x = 0; x < width; x++){
		for (int y = 0; y < height; y++){
			float dx = (*gx)(x, y) / 255;
            float dy = (*gy)(x, y) / 255;
			float dz = 1/strength;

			float d = sqrt(dx*dx + dy*dy + dz*dz);
            dx = (dx / d + 1.0) * (255 / strength);
            dy = (dy / d + 1.0) * (255 / strength);
            dz = (dz / d + 1.0) * (255 / strength);

			newImage->setPixel(x, y, qRgb(dx, dy, dz));

		}
    }

    return newImage;
}
