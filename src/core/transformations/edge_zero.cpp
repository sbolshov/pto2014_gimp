#include "edge_zero.h"

#include "edge_laplacian_of_gauss.h"

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img) :
    Convolution(img)
{
}

EdgeZeroCrossing::EdgeZeroCrossing(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}


double getMin(math::matrix<float> matrix, int size) {
    double min = 255.0;
    for(int x = 0; x < size; x++) {
        for(int y = 0; y < size; y++) {
            if(matrix(x,y)<min) {
                min = matrix(x,y);
            }
        }
    }
    return min;
}

double getMax(math::matrix<float> matrix, int size) {
    double max = 0.0;
    for(int x = 0; x < size; x++) {
        for(int y = 0; y < size; y++) {
            if(matrix(x,y)>max) {
                max = matrix(x,y);
            }
        }
    }
    return max;
}

PNM* EdgeZeroCrossing::transform()
{
    int width = image->width(),
        height = image->height();

    int    size  = getParameter("size").toInt();
    double sigma = getParameter("sigma").toDouble();
    int    t     = getParameter("threshold").toInt();

    EdgeLaplaceOfGauss laplaceOfGauss(image);
    laplaceOfGauss.setParameter("size", size);
    laplaceOfGauss.setParameter("sigma", sigma);

    PNM* newImage = laplaceOfGauss.transform();

    Transformation laplace(laplaceOfGauss.transform());


    int v0 = 128;

    if(image->format() == QImage::Format_Indexed8) {
        for(int x=0; x<width; x++) {
            for(int y=0; y<height; y++) {
                math::matrix<float> maska = laplace.getWindow(x,y,size,LChannel,NullEdge);
                double max = getMax(maska, size);
                double min = getMin(maska, size);
                if((min < (v0 - t)) && (max > (v0 + t))) {
                    int q = maska(size/2,size/2);
                    newImage->setPixel(x,y,q);
                } else {
                    newImage->setPixel(x,y,0);
                }
            }
        }
    } else {
        for(int x=0; x<width; x++) {
            for(int y=0; y<height; y++) {
                math::matrix<float> maska = laplace.getWindow(x,y,size, RChannel, NullEdge);
                double max = getMax(maska, size);
                double min = getMin(maska, size);
                int r,g,b;
                if((min < (v0 - t)) && (max > (v0 + t))) {
                    r = maska(size/2,size/2);
                } else {
                    r = 0;
                }
                maska = laplace.getWindow(x,y,size, GChannel, NullEdge);
                max = getMax(maska, size);
                min = getMin(maska, size);
                if((min < (v0 - t)) && (max > (v0 + t))) {
                    g = maska(size/2,size/2);
                } else {
                    g = 0;
                }
                maska = laplace.getWindow(x,y,size, BChannel, NullEdge);
                max = getMax(maska, size);
                min = getMin(maska, size);
                if((min < (v0 - t)) && (max > (v0 + t))) {
                    b = maska(size/2,size/2);
                } else {
                    b = 0;
                }
                newImage->setPixel(x, y, qRgb(r,g,b));
            }
        }
    }

    return newImage;
}

