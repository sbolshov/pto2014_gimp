#include "histogram_stretching.h"

#include "../histogram.h"

HistogramStretching::HistogramStretching(PNM* img) :
    Transformation(img)
{
}

HistogramStretching::HistogramStretching(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

int HistogramStretching::getMax(QHash<int,int>* histogram) {
    int max = 0;
	QHash<int, int>::const_iterator i = histogram->constBegin();
	while (i != histogram->constEnd()) {
		if (i.value() > 0) {
			if (i.key() > max) {
				max = i.key();
			}
		}
		++i;
	}
    return max;
}

int HistogramStretching::getMin(QHash<int,int>* histogram) {
	int min = getMax(histogram);
	QHash<int, int>::const_iterator i = histogram->constBegin();
	while (i != histogram->constEnd()) {
		if (i.value() > 0) {
			if (i.key() < min) {
				min = i.key();
			}
		}
		++i;
	}
	return min;
}

PNM* HistogramStretching::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8) {
        int min = this->getMin(image->getHistogram()->get(Histogram::LChannel));
        int max = this->getMax(image->getHistogram()->get(Histogram::LChannel));
        qDebug()<<min<<":"<<max;
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				QRgb pixel = image->pixel(x, y);
                int v = qGray(pixel);
                v = ((float)255 / (max - min)) * (v - min);
				newImage->setPixel(x,y,v);
			}
		}
    } else {
        QHash<int,int>* rChanel = image->getHistogram()->get(Histogram::RChannel);
        QHash<int,int>* gChanel = image->getHistogram()->get(Histogram::GChannel);
        QHash<int,int>* bChanel = image->getHistogram()->get(Histogram::BChannel);
        QHash<int,int>* lChanel = image->getHistogram()->get(Histogram::LChannel);
        int rmin = this->getMin(rChanel);
        int gmin = this->getMin(gChanel);
        int bmin = this->getMin(bChanel);
        int lmin = this->getMin(lChanel);
        int rmax = this->getMax(rChanel);
        int gmax = this->getMax(gChanel);
        int bmax = this->getMax(bChanel);
        int lmax = this->getMax(lChanel);
        for(int x=0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                QRgb pixel = image->pixel(x,y);
                int r = qRed(pixel);    // Get the 0-255 value of the R channel
                int g = qGreen(pixel);  // Get the 0-255 value of the G channel
                int b = qBlue(pixel);   // Get the 0-255 value of the B channel
                int l = qGray(pixel);
                r = ((float)255/(rmax - rmin)) * (r - rmin);
                g = ((float)255/(gmax - gmin)) * (g - gmin);
                b = ((float)255/(bmax - bmin)) * (b - bmin);
                l = ((float)255/(lmax - lmin)) * (l - lmin);
                QColor newPixel = QColor(r,g,b, l);
                newImage->setPixel(x,y, newPixel.rgb());
            }
        }
    }


    return newImage;
}


