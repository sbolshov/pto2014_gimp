#include "bin_gradient.h"

BinarizationGradient::BinarizationGradient(PNM* img) :
    Transformation(img)
{
}

BinarizationGradient::BinarizationGradient(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* BinarizationGradient::transform()
{
    int width = image->width();
    int height = image->height();
    int threshold;

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    int numerator = 0;
    int denominator = 0;

    for(int x=0; x < width; x++) {
        for(int y=0; y < height; y++) {
            QRgb pixel = image->pixel(x,y); // Getting the pixel(x,y) value
            int l = qGray(pixel);
            int Gx = 0;
            int Gy = 0;
            int Gmax;
            if(x+1 < width) {
                Gx = qGray(image->pixel(x + 1,y));
            }
            if(x-1 > 0) {
                Gx = Gx - qGray(image->pixel(x - 1,y));
            }
            if(y+1 < height) {
                Gy = qGray(image->pixel(x,y + 1));
            }
            if(y-1 > 0) {
                Gy = Gy - qGray(image->pixel(x,y - 1));
            }
            Gmax = Gx;
            if(Gmax > Gy) {
                Gmax = Gy;
            }
            numerator += Gmax * l;
            denominator += Gmax;
        }
    }

    threshold = (int)((float)numerator/denominator);

    for(int x=0; x < width; x++) {
        for(int y=0; y < height; y++) {
            if(qGray(image->pixel(x,y)) >= threshold) {
                newImage->setPixel(x,y, Qt::color1);
                continue;
            }
            newImage->setPixel(x,y, Qt::color0);
        }
    }

    return newImage;
}


