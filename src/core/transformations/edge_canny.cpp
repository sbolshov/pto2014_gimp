#include "edge_canny.h"

#include "blur_gaussian.h"
#include "conversion_grayscale.h"
#include "edge_sobel.h"

EdgeCanny::EdgeCanny(PNM* img) :
    Convolution(img)
{
}

EdgeCanny::EdgeCanny(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
	int width = image->width(),
		height = image->height();
	grMagn = math::matrix<float>(width, height);
	grOrient = math::matrix<float>(width, height);
}

PNM* EdgeCanny::transform()
{
	int width = image->width(),
		height = image->height();

    int upper_thresh = getParameter("upper_threshold").toInt(),
        lower_thresh = getParameter("lower_threshold").toInt();

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);
	//PNM* newImage = new PNM(width, height, image->format());
	PNM* tempImage = new PNM(width, height, QImage::Format_Indexed8); 
	tempImage = ConversionGrayscale(image).transform();
	BlurGaussian gauss(tempImage);
	gauss.setParameter("size", 3);
	gauss.setParameter("sigma", 1.6);
	tempImage = gauss.transform();
	EdgeSobel sobel(tempImage);
	math::matrix<float>* gx = sobel.rawHorizontalDetection();
	math::matrix<float>* gy = sobel.rawVerticalDetection();
	
	//moc i kierunek gradientu
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {
			float tempx = (*gx)(x, y);
			float tempy = (*gy)(x, y);
			grMagn(x, y) = sqrt(pow(tempx, 2) + pow(tempy, 2));
			grOrient(x,y)= (atan2(tempx, tempy) / 3.14 * 180 + 360) / 360;
			newImage->setPixel(x, y, 0);
		}
	}
	
	
	std::list<QPoint> inits;
	
	//tlumienie niemaksymalne-pkt 5
	for (int x = 1; x < width-1; x++) {
		for (int y = 1; y < height-1; y++) {
			
			int x1, y1, x2, y2 = 0;

			if ((grOrient(x, y) >= 0 && grOrient(x, y) <= 22) || (grOrient(x, y) >= 158 && grOrient(x, y) <= 202) || (grOrient(x, y) >= 338 && grOrient(x, y) <= 360)) {
				x1 = (x - 1 < 0 ? 0 : x - 1);
				y1 = y;
				x2 = (x + 1 > width ? width - 1 : x + 1);
				y2 = y;
			}
			if ((grOrient(x, y) >= 23 && grOrient(x, y) <= 67) || (grOrient(x, y) >= 203 && grOrient(x, y) <= 247)) {
				x1 = (x - 1 < 0 ? 0 : x - 1);
				y1 = (y - 1 < 0 ? 0 : y - 1);
				x2 = (x + 1 > width ? width - 1 : x + 1);
				y2 = (y + 1 > height ? height - 1 : y + 1);
			}
			if ((grOrient(x, y) >= 68 && grOrient(x, y) <= 112) || (grOrient(x, y) >= 248 && grOrient(x, y) <= 292)) {
				x1 = x;
				y1 = (y - 1 < 0 ? 0 : y - 1);
				x2 = x;
				y2 = (y + 1 > height ? height - 1 : y + 1);
			}
			if ((grOrient(x, y) >= 113 && grOrient(x, y) <= 157) || (grOrient(x, y) >= 293 && grOrient(x, y) <= 337)) {
				x1 = (x + 1 > width ? width - 1 : x + 1);
				y1 = (y + 1 > height ? height - 1 : y + 1);
				x2 = (x - 1 < 0 ? 0 : x - 1);
				y2 = (y - 1 < 0 ? 0 : y - 1);
			}
			
			if ((grMagn(x, y) > grMagn(x1, y1)) && (grMagn(x, y) > grMagn(x2, y2)) && (grMagn(x, y) > upper_thresh)) {
				newImage->setPixel(x, y, 255);
				inits.push_back(QPoint(x, y));
			}
		}
	}
	
	//progowanie z histerez�-pkt 6
	while (!inits.empty()) {
		QPoint point = inits.front();
		inits.pop_front();
		int t = grOrient(point.x(), point.y());

		//w poziomie 
		if ((t >= 0 && t <= 22) || (t >= 158 && t <= 202) || (t >= 338 && t <= 360)) {
			int j = point.y();
			//sprawdza w lewo
			for (int i = point.x() - 1; i > 0; i--) {
				if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t || grMagn(i - 1, j) < grMagn(i, j))){
					break;
				}
				newImage->setPixel(i, j, 255);
			}
			//sprawdza w prawo
			for (int i = point.x() + 1; i < width; i++) {
				if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t) || (grMagn(i + 1, j) < grMagn(i, j))) {
					break;
				}
				newImage->setPixel(i, j, 255);
			}
		}

		//po skosie - z lewego dolu do prawej gory
		if ((t >= 23 && t <= 67) || (t >= 203 && t <= 247)) {
			//sprawdza w lewo w dol
			for (int i = point.x() - 1; i > 0; i--) {
				for (int j = point.y() + 1; j < height; j++) {
					if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t) || (grMagn(i - 1, j + 1) < grMagn(i, j))) {
						break;
					}
					newImage->setPixel(i, j, 255);
				}
			}
			//sprawdza w prawo w gore
			for (int i = point.x() + 1; i < width; i++) {
				for (int j = point.y() - 1; j > 0; j--) {
					if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t) || (grMagn(i + 1, j - 1) < grMagn(i, j))) {
						break;
					}
					newImage->setPixel(i, j, 255);
				}
			}
		}

		//w pionie
		if ((t >= 68 && t <= 112) || (t >= 248 && t <= 292)) {
			int i = point.x();
			//sprawdza w dol
			for (int j = point.y() + 1; j < height; j++) {
				if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t) || (grMagn(i, j + 1) < grMagn(i, j))) {
					break;
				}
				newImage->setPixel(i, j, 255);
			}
			//sprawdza w gore
			for (int j = point.y() - 1; j > 0; j--) {
				if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t) || (grMagn(i, j - 1) < grMagn(i, j))) {
					break;
				}
				newImage->setPixel(i, j, 255);
			}
		}

		//po skosie - z lewej gory na prawy dol
		if ((grOrient(point.x(), point.y()) >= 113 && grOrient(point.x(), point.y()) <= 157) || (grOrient(point.x(), point.y()) >= 293 && grOrient(point.x(), point.y()) <= 337)) {
			//sprawdza w lewo w gore
			for (int i = point.x() - 1; i > 0; i--) {
				for (int j = point.y() - 1; j > 0; j--) {
					if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t) || (grMagn(i - 1, j - 1) < grMagn(i, j))) {
						break;
					}
					newImage->setPixel(i, j, 255);
				}
			}
			//sprawdza w prawo w dol
			for (int i = point.x() + 1; i < width; i++) {
				for (int j = point.y() + 1; j < height; j++) {
					if ((newImage->pixel(i, j) > 0) || (grMagn(i, j) < lower_thresh) || (grMagn(i, j) != t) || (grMagn(i + 1, j + 1) < grMagn(i, j))) {
						break;
					}
					newImage->setPixel(i, j, 255);

				}
			}
		}
	}
	

    return newImage;
}
