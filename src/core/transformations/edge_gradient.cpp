#include "edge_gradient.h"

EdgeGradient::EdgeGradient(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

EdgeGradient::EdgeGradient(PNM* img) :
    Convolution(img)
{
}

PNM* EdgeGradient::verticalDetection()
{
    return convolute(g_y, RepeatEdge);
}

PNM* EdgeGradient::horizontalDetection()
{
    return convolute(g_x, RepeatEdge);
}

PNM* EdgeGradient::transform()
{
    PNM* newImage = new PNM(image->width(), image->height(), image->format());
    PNM* hDetection = horizontalDetection();
    PNM* vDetection = verticalDetection();

    for (int x = 0; x < image->width(); x++) {
        for (int y = 0; y < image->height(); y++) {
            QRgb hpixel = hDetection->pixel(x, y); // Getting the pixel(x,y) value
            QRgb vpixel = vDetection->pixel(x, y);
            if(image->format() == QImage::Format_RGB32) {
                int hr = qRed(hpixel);    // Get the 0-255 value of the R channel
                int hg = qGreen(hpixel);  // Get the 0-255 value of the G channel
                int hb = qBlue(hpixel);   // Get the 0-255 value of the B channel
                int vr = qRed(vpixel);    // Get the 0-255 value of the R channel
                int vg = qGreen(vpixel);  // Get the 0-255 value of the G channel
                int vb = qBlue(vpixel);   // Get the 0-255 value of the B channel
                int r = (int)sqrt(hr*hr+vr*vr);
                int g = (int)sqrt(hg*hg+vg*vg);
                int b = (int)sqrt(hb*hb+vb*vb);
                if(r<0) {
                    r = 0;
                } else if (r>255) {
                    r = 255;
                }
                if(g<0) {
                    g = 0;
                } else if (g>255) {
                    g = 255;
                }
                if(b<0) {
                    b = 0;
                } else if (b>255) {
                    b = 255;
                }
                QColor newPixel = QColor(r, g, b);
                newImage->setPixel(x,y, newPixel.rgb());
            } else if(image->format() == QImage::Format_Indexed8) {
                int hl = qGray(hpixel);   // Get the 0-255 value of the L channel
                int vl = qGray(vpixel);   // Get the 0-255 value of the L channel
                int l = (int)sqrt(hl*hl+vl*vl);
                if(l<0) {
                    l = 0;
                } else if (l>255) {
                    l = 255;
                }
                newImage->setPixel(x,y,l);
            }
        }
    }

    return newImage;
}

