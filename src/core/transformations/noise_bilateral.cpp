#include "noise_bilateral.h"

NoiseBilateral::NoiseBilateral(PNM* img) :
    Convolution(img)
{
}

NoiseBilateral::NoiseBilateral(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseBilateral::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    sigma_d = getParameter("sigma_d").toInt();
    sigma_r = getParameter("sigma_r").toInt();
    radius = sigma_d;

	if (image->format() == QImage::Format_Indexed8)
	{
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
				newImage->setPixel(x, y, calcVal(x, y, LChannel));
	}
	else
	{
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
			{
				int r = calcVal(x, y, RChannel);
				int g = calcVal(x, y, GChannel);
				int b = calcVal(x, y, BChannel);
				newImage->setPixel(x,y,QColor(r, g, b).rgb());
			}
	}

    return newImage;
}

int NoiseBilateral::calcVal(int x, int y, Channel channel)
{
	math::matrix<float> uokno = getWindow(x, y, sigma_d * 2 + 1, channel, RepeatEdge);
	float gora = 0;
	float dol = 0;
    for (uint i = 0; i < uokno.rowno(); i++)
        for (uint j = 0; j < uokno.colno(); j++)
		{
			int temp;
			switch (channel)
			{
			case RChannel:
				temp = qRed(image->pixel(x, y));
				break;
			case GChannel:
				temp = qGreen(image->pixel(x, y));
				break;
			case BChannel:
				temp = qBlue(image->pixel(x, y));
				break;
			default:
				temp = qGray(image->pixel(x, y));
				break;
			}

			int px = x + (i - (int)((radius * 2 + 1) / 2));
			if (px <= 0) px = 0;
			if (px >= 255) px = 255;
			int py = y + (j - (int)((radius * 2 + 1) / 2));
			if (py <= 0) py = 0;
			if (py >= 255) py = 255;

			float cc = colorCloseness(uokno(i, j), temp);
			float sc = spatialCloseness(QPoint(px, py), QPoint(x, y));

			//qDebug() << "colorCloseness " << cc << " spatialCloseness " << sc;
			gora += uokno(i, j) * cc * sc;
			dol += cc * sc;
		}
    return (int)gora/dol;
}

float NoiseBilateral::colorCloseness(int val1, int val2)
{
	return pow(exp(1), -(pow(val1 - val2, 2) / (2 * pow(sigma_r, 2))));
}

float NoiseBilateral::spatialCloseness(QPoint point1, QPoint point2)
{
	return pow(exp(1), -((pow(point1.x() - point2.x(), 2) + pow(point1.y() - point2.y(), 2)) / (2 * pow(sigma_d, 2))));
}
