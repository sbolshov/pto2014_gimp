#include "noise_median.h"

NoiseMedian::NoiseMedian(PNM* img) :
    Convolution(img)
{
}

NoiseMedian::NoiseMedian(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* NoiseMedian::transform()
{
    int width  = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    for(int x=0; x < width; x++) {
        for(int y=0; y < height; y++) {
            if(image->format() == QImage::Format_RGB32) {
                int r = getMedian(x,y,RChannel);
                int g = getMedian(x,y,GChannel);
                int b = getMedian(x,y,BChannel);
                QColor newPixel = QColor(r,g,b);
                newImage->setPixel(x,y, newPixel.rgb());
            } else if(image->format() == QImage::Format_Indexed8) {
                newImage->setPixel(x,y, getMedian(x,y,LChannel));
            }
        }
    }

    return newImage;
}

int NoiseMedian::getMedian(int x, int y, Channel channel)
{
    int radius = getParameter("radius").toInt();

    Mode mode= RepeatEdge;

    int size = radius*2 + 1;

    math::matrix<float> window(size,size);

    float pixels[(25*2+1)*(25*2+1)];
    int index = 0;
    window = getWindow(x,y,radius*2+1, channel, mode);
    for(int i=0; i<size; i++) {
        for(int j=0; j<size; j++) {
           pixels[index] = window(i,j);
           ++index;
        }
    }
    std::sort(pixels, pixels+index);
    return pixels[index/2];
}
