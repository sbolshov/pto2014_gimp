#include "map_horizon.h"

#include "map_height.h"

MapHorizon::MapHorizon(PNM* img) :
    Transformation(img)
{
}

MapHorizon::MapHorizon(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* MapHorizon::transform()
{
    int width  = image->width(),
        height = image->height();

    double scale     = getParameter("scale").toDouble();
    int    sunAlpha = getParameter("alpha").toInt();
    int dx, dy;

    switch (getParameter("direction").toInt())
    {
    case NORTH: dx = 0; dy = -1; break;
    case SOUTH: dx = 0; dy = 1; break;
    case EAST:  dx = 1; dy = 0; break;
    case WEST:  dx = -1; dy = 0; break;
    default:
        qWarning() << "Unknown direction!";
        dx =  0;
        dy = -1;
    }

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);
	PNM* mapHeightImage = MapHeight(image).transform();
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {
			float alpha = 0;
			int currentH = qGray(mapHeightImage->pixel(x, y));
			for (int i = x + dx, j = y + dy; i < width && j < height && i >= 0 && j >= 0; i = i + dx, j = j + dy) {
				int rayH = qGray(mapHeightImage->pixel(i, j));
				if (currentH < rayH) {
					float distance = (sqrt((x - i)*(x - i) + (y - j)*(y - j))*scale);
					float rayAlpha = atan((rayH - currentH) / distance);
					if (rayAlpha > alpha) {
						alpha = rayAlpha;
					}
				}
			}
			float delta = alpha - sunAlpha * 3.14 / 180;
			if (delta > 0) {
				newImage->setPixel(x, y, cos(delta) * 255);
			}
			else {
				newImage->setPixel(x, y, 255);
			}
		}
	}
    return newImage;
}
