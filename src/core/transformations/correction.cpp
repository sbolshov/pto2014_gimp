#include "correction.h"
#include <math.h>

Correction::Correction(PNM* img) :
    Transformation(img)
{
}

Correction::Correction(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

PNM* Correction::transform()
{
    float shift  = getParameter("shift").toFloat();
    float factor = getParameter("factor").toFloat();
    float gamma  = getParameter("gamma").toFloat();

    int width  = image->width();
    int height = image->height();

	for (int i = 0; i < 256; i++) {
        this->LUT[i] = (int)pow((i + shift) * factor, gamma);
        if (this->LUT[i] > 255) this->LUT[i] = 255;
        if (this->LUT[i] < 0) this->LUT[i] = 0;
	}

    PNM* newImage = new PNM(width, height, image->format());

	if (image->format() == QImage::Format_Indexed8)
	{
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
			{
			QRgb pixel = image->pixel(x, y);
			int v = qGray(pixel);
            newImage->setPixel(x, y, this->LUT[v]);
			}
	}

	else
    {
		for (int x = 0; x < width; x++)
			for (int y = 0; y < height; y++)
			{
			QRgb pixel = image->pixel(x, y); // Getting the pixel(x,y) value

			int r = qRed(pixel);    // Get the 0-255 value of the R channel
			int g = qGreen(pixel);  // Get the 0-255 value of the G channel
			int b = qBlue(pixel);   // Get the 0-255 value of the B channel
            QColor newPixel = QColor(this->LUT[r], this->LUT[g], this->LUT[b]);
			newImage->setPixel(x, y, newPixel.rgb());
			}
	}
	

    return newImage;
}
