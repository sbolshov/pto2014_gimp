#include "edge_sobel.h"

EdgeSobel::EdgeSobel(PNM* img, ImageViewer* iv) :
    EdgeGradient(img, iv)
{
    prepareMatrices();
}

EdgeSobel::EdgeSobel(PNM* img) :
    EdgeGradient(img)
{
    prepareMatrices();
}

void EdgeSobel::prepareMatrices()
{
    math::matrix<float> gx(3,3);
    math::matrix<float> gy(3,3);

    gx(0,0) = -1;
    gx(0,1) = 0;
    gx(0,2) = 1;
    gx(1,0) = -2;
    gx(1,1) = 0;
    gx(1,2) = 2;
    gx(2,0) = -1;
    gx(2,1) = 0;
    gx(2,2) = 1;

    gy(0,0) = -1;
    gy(0,1) = -2;
    gy(0,2) = -1;
    gy(1,0) = 0;
    gy(1,1) = 0;
    gy(1,2) = 0;
    gy(2,0) = 1;
    gy(2,1) = 2;
    gy(2,2) = 1;

    g_x = gx;
    g_y = gy;
}

math::matrix<float>* EdgeSobel::rawHorizontalDetection()
{
    math::matrix<float>* x_gradient = new math::matrix<float>(this->image->width(), this->image->height());

	for (int x = 0; x<image->width(); x++){
		for (int y = 0; y<image->height(); y++){
			math::matrix<float> window = getWindow(x, y, 3, LChannel, RepeatEdge);
			(*x_gradient)(x, y) = sum(join(g_x, window));
		}
	}
	return x_gradient;
}

math::matrix<float>* EdgeSobel::rawVerticalDetection()
{
    math::matrix<float>* y_gradient = new  math::matrix<float>(this->image->width(), this->image->height());

	for (int x = 0; x < image->width(); x++){
		for (int y = 0; y < image->height(); y++){
			math::matrix<float> window = getWindow(x, y, 3, LChannel, RepeatEdge);
			(*(y_gradient))(x, y) = sum(join(g_y, window));
		}
	}
    return y_gradient;
}
