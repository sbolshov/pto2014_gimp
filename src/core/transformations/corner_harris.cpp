#include "corner_harris.h"

#include "blur_gaussian.h"
#include "conversion_grayscale.h"
#include "edge_sobel.h"

CornerHarris::CornerHarris(PNM* img) :
    Convolution(img)
{
}

CornerHarris::CornerHarris(PNM* img, ImageViewer* iv) :
    Convolution(img, iv)
{
}

PNM* CornerHarris::transform()
{
    int    threshold    = getParameter("threshold").toInt();
    double sigma        = getParameter("sigma").toDouble(),
           sigma_weight = getParameter("sigma_weight").toDouble(),
           k_param      = getParameter("k").toDouble();

    int width  = image->width(),
        height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Mono);

    math::matrix<double> Ixx(width, height);
    math::matrix<double> Iyy(width, height);
    math::matrix<double> Ixy(width, height);
    math::matrix<double> corners_candidates(width, height);
    math::matrix<double> corners_nonmax_suppress(width, height);

    for(int x=0; x < width; x++) {
        for(int y=0; y < height; y++) {
            corners_candidates(x,y) = 0;
            corners_nonmax_suppress(x,y) = 0;
        }
    }

    PNM* tempImage = new PNM(width, height, QImage::Format_Indexed8);
    tempImage = ConversionGrayscale(image).transform();
    BlurGaussian gauss(tempImage);
    gauss.setParameter("size", 3);
    gauss.setParameter("sigma", 3.6);
    tempImage = gauss.transform();
    EdgeSobel sobel(tempImage);
    math::matrix<float>* Gx = sobel.rawHorizontalDetection();
    math::matrix<float>* Gy = sobel.rawVerticalDetection();


    for(int x=0; x < width; x++) {
        for(int y=0; y<height; y++) {
            double gx = (double)(*Gx)(x,y);
            double gy = (double)(*Gy)(x,y);
            Ixx(x,y) = gx*gx;
            Iyy(x,y) = gy*gy;
            Ixy(x,y) = gx*gy;
        }
    }

    for(int x=1; x < width-1; x++) {
        for(int y=1; y<height-1; y++) {
            double Sxx = 0;
            double Syy = 0;
            double Sxy = 0;

            for(int k = -1; k < 2; k++) {
                for(int l = -1; l < 2; l++) {
                    Sxx += Ixx(x+k,y+l) * BlurGaussian::getGauss(k,l,sigma);
                    Syy += Iyy(x+k,y+l) * BlurGaussian::getGauss(k,l,sigma);
                    Sxy += Ixy(x+k,y+l) * BlurGaussian::getGauss(k,l,sigma);
                }
            }

            Sxx /=sigma_weight;
            Syy /=sigma_weight;
            Sxy /=sigma_weight;

            double r = Sxx*Syy - Sxy*Sxy - k_param * (Sxx + Syy) * (Sxx + Syy);

            if (r > threshold) {
                corners_candidates(x,y) = r;
            }

        }
    }

    bool search = true;
    while (search) {
        search = false;
        for(int x=1; x< width-1; x++) {
            for(int y=1; y< height - 1; y++) {
                double center = corners_candidates(x,y);
                if(center > corners_candidates(x-1,y-1) && center > corners_candidates(x, y-1) && center > corners_candidates(x+1,y-1) && center>corners_candidates(x+1,y) && center>corners_candidates(x+1,y+1) && center> corners_candidates(x,y+1) && center>corners_candidates(x-1,y+1) && center>corners_candidates(x-1,y)) {
                    corners_nonmax_suppress(x,y) = center;
                } else {
                    if(center > 0) {
                        search = true;
                    }
                }
            }
        }
        corners_candidates = corners_nonmax_suppress;
    }


    for(int x=0; x < width; x++) {
        for(int y=0; y<height; y++) {
            if(corners_candidates(x,y) == 0) {
                newImage->setPixel(x,y, Qt::color0);
            } else {
                newImage->setPixel(x,y, Qt::color1);
            }
        }
    }


    return newImage;
}
