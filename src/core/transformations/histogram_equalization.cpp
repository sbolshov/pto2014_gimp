#include "histogram_equalization.h"

#include "../histogram.h"

HistogramEqualization::HistogramEqualization(PNM* img) :
    Transformation(img)
{
}

HistogramEqualization::HistogramEqualization(PNM* img, ImageViewer* iv) :
    Transformation(img, iv)
{
}

int HistogramEqualization::getSum(QHash<int, int>* histogram, int p) {
    int sum = 0;
    QHash<int, int>::const_iterator i = histogram->constBegin();
    while (i != histogram->constEnd()) {
        if(i.key() < p) {
            sum = sum + i.value();
        }
        ++i;
    }
    return sum;
}

int HistogramEqualization::getFirstNoneZero(double *d) {
    int n=0;
    while (d[n] <= 0) {
        n++;
    }
    return d[n];
}

PNM* HistogramEqualization::transform()
{
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, image->format());

    if (image->format() == QImage::Format_Indexed8) {
        QHash<int,int>* lChanel = image->getHistogram()->get(Histogram::LChannel);
        double lD[PIXEL_VAL_MAX+1];
        int lLUT[PIXEL_VAL_MAX+1];
        int nop = width * height;

        for(int p=0; p < 255; p++) {
            lD[p] = ((double) this->getSum(lChanel, p)) / nop;
        }

        int minlD = this->getFirstNoneZero(lD);

        for(int p=0; p < 255; p++) {
            lLUT[p] = floor((lD[p] - minlD) * 255/(1 - minlD));
        }

        for(int x=0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                QRgb pixel = image->pixel(x, y); // Getting the pixel(x,y) value
                int l = qGray(pixel);   // Get the 0-255 value of the L channel
                newImage->setPixel(x, y,  lLUT[l]);
            }
        }
    } else {
        QHash<int,int>* rChanel = image->getHistogram()->get(Histogram::RChannel);
        QHash<int,int>* gChanel = image->getHistogram()->get(Histogram::GChannel);
        QHash<int,int>* bChanel = image->getHistogram()->get(Histogram::BChannel);
        QHash<int,int>* lChanel = image->getHistogram()->get(Histogram::LChannel);
        double rD[PIXEL_VAL_MAX+1];
        double gD[PIXEL_VAL_MAX+1];
        double bD[PIXEL_VAL_MAX+1];
        double lD[PIXEL_VAL_MAX+1];
        int rLUT[PIXEL_VAL_MAX+1];
        int gLUT[PIXEL_VAL_MAX+1];
        int bLUT[PIXEL_VAL_MAX+1];
        int lLUT[PIXEL_VAL_MAX+1];
        int nop = width * height;
        for(int p=0; p < 255; p++) {
            rD[p] = ((double) this->getSum(rChanel, p)) / nop;
            gD[p] = ((double) this->getSum(gChanel, p)) / nop;
            bD[p] = ((double) this->getSum(bChanel, p)) / nop;
            lD[p] = ((double) this->getSum(lChanel, p)) / nop;
        }

        int minrD = this->getFirstNoneZero(rD);
        int mingD = this->getFirstNoneZero(gD);
        int minbD = this->getFirstNoneZero(bD);
        int minlD = this->getFirstNoneZero(lD);

        for(int p=0; p < 255; p++) {
            rLUT[p] = floor((rD[p] - minrD) * 255/(1 - minrD));
            gLUT[p] = floor((gD[p] - mingD) * 255/(1 - mingD));
            bLUT[p] = floor((bD[p] - minbD) * 255/(1 - minbD));
            lLUT[p] = floor((lD[p] - minlD) * 255/(1 - minlD));
        }

        for(int x=0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                QRgb pixel = image->pixel(x, y); // Getting the pixel(x,y) value

                int r = qRed(pixel);    // Get the 0-255 value of the R channel
                int g = qGreen(pixel);  // Get the 0-255 value of the G channel
                int b = qBlue(pixel);   // Get the 0-255 value of the B channel
                int l = qGray(pixel);   // Get the 0-255 value of the L channel
                QColor newPixel = QColor(rLUT[r], gLUT[g], bLUT[b], lLUT[l]);
                newImage->setPixel(x, y, newPixel.rgb());
            }
        }
    }

    return newImage;
}

