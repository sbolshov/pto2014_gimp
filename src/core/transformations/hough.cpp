#include "hough.h"

#include "conversion_grayscale.h"
#include "edge_laplacian.h"

Hough::Hough(PNM* img) :
    Transformation(img)
{
}

Hough::Hough(PNM* img, ImageViewer* super) :
    Transformation(img, super)
{
}

PNM* Hough::transform()
{   
    int thetaDensity = getParameter("theta_density").toInt();
    int width = image->width();
    int height = image->height();

    PNM* newImage = new PNM(width, height, QImage::Format_Indexed8);

    newImage = ConversionGrayscale(image).transform();

    if(getParameter("skip_edge_detection").toBool() == false) {
        EdgeLaplacian laplace(newImage);
        laplace.setParameter("size", 3);
        newImage = laplace.transform();
    }

    int pmax = sqrt(width*width + height*height);
    int thetaSize = 180 * thetaDensity;

    PNM* endImage = new PNM(thetaSize, pmax * 2 + 1, QImage::Format_Indexed8);

    math::matrix<int> hough(thetaSize, pmax * 2 + 1);
    for(int x = 0; x < thetaSize; x++) {
        for(int y = 0; y<(pmax * 2 + 1); y++) {
            hough(x,y) = 0;
        }
    }

    for(int x=0; x<width; x++) {
        for(int y=0;y<height; y++) {
            QRgb pixel = newImage->pixel(x,y);
            if(qGray(pixel) > 0) {
                for(int k=0; k<thetaSize;k++) {
                    double theta = (k*M_PI)/(180.0*thetaDensity);
                    double rho = x*cos(theta) + y*sin(theta);
                    hough(k, round(rho + pmax))++;
                }
            }
        }
    }

    int max = hough.max();

    for(int x=0; x<thetaSize; x++) {
        for(int y=0;y<pmax*2+1; y++) {
            endImage->setPixel(x,y, round(255.0*hough(x,y)/max));
        }
    }

    return endImage;
}
